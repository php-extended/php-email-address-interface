<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use InvalidArgumentException;
use PhpExtended\Domain\DomainInterface;
use Stringable;

/**
 * MailboxInterface interface file.
 * 
 * This interface specifies how a mailbox (meaning "NAME <email@domain>") should
 * be handled.
 * 
 * Mailboxes are considered immutable; all methods that might change state
 * MUST be implemented such that they retain the internal state of the current
 * mailbox and return an instance that contains the changed state.
 * 
 * @author Anastaszor
 */
interface MailboxInterface extends Stringable
{
	
	/**
	 * Gets the display name of the mailbox.
	 * 
	 * @return string
	 */
	public function getDisplayName() : string;
	
	/**
	 * Sets the display name of the mailbox.
	 * 
	 * @param string $displayName
	 * @return MailboxInterface
	 */
	public function withDisplayName(string $displayName) : MailboxInterface;
	
	/**
	 * Gets the email address of the mailbox.
	 * 
	 * @return EmailAddressInterface
	 */
	public function getEmailAddress() : EmailAddressInterface;
	
	/**
	 * Sets the email aaddress of the mailbox.
	 * 
	 * @param EmailAddressInterface $email
	 * @return MailboxInterface
	 */
	public function withEmailAddress(EmailAddressInterface $email) : MailboxInterface;
	
	/**
	 * Sets the local part of the email address. If the local part is not
	 * conform to rfc5322 (3.4.1 Addr-Spec), then an InvalidArgumentException
	 * is thrown.
	 * 
	 * @param string $local
	 * @return MailboxInterface
	 * @throws InvalidArgumentException if the local part is not acceptable
	 */
	public function withEmailAddressLocalPart(string $local) : MailboxInterface;
	
	/**
	 * Sets the domain part of the email address. If the domain part is not
	 * conform to rfc5322 (3.4.1 Addr-Spec), then an InvalidArgumentException
	 * is thrown.
	 * 
	 * @param DomainInterface $domain
	 * @return MailboxInterface
	 * @throws InvalidArgumentException if the domain part is not acceptable
	 */
	public function withEmailAddressDomain(DomainInterface $domain) : MailboxInterface;
	
	/**
	 * Gets a prefectly well quoted string that correspond to a canonical
	 * representation of this mailbox.
	 * 
	 * @return string
	 */
	public function getCanonicalRepresentation() : string;
	
	/**
	 * Gets whether this mailbox equals another mailbox, meaning they have the
	 * same display name and their email addresses are equals.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $other
	 * @return boolean
	 */
	public function equals($other) : bool;
	
	/**
	 * Gets whether this mailbox contains the given email address.
	 *
	 * @param EmailAddressInterface $address
	 * @return boolean
	 */
	public function containsEmailAddress(EmailAddressInterface $address) : bool;
	
}

<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use Countable;
use Iterator;
use PhpExtended\Domain\DomainInterface;
use Stringable;

/**
 * MailboxGroupInterface interface file.
 * 
 * This interface specifies how a list of mailboxes should be handled.
 * 
 * Mailbox address lists are considered immutable; all methods that might change
 * state MUST be implemented such that they retain the internal state of the
 * current list and return an instance that contains the changed state.
 * 
 * @author Anastaszor
 * @extends \Iterator<int, MailboxInterface>
 */
interface MailboxGroupInterface extends Countable, Iterator, Stringable
{
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 */
	public function current() : MailboxInterface;
	
	/**
	 * Creates a new MailboxGroupInterface with all the email address domains
	 * converted to the given domain.
	 *
	 * @param DomainInterface $domain
	 * @return MailboxGroupInterface
	 */
	public function withDomain(DomainInterface $domain) : MailboxGroupInterface;
	
	/**
	 * Creates a new MailboxGroupInterface where all the email address are
	 * the email addresses which have the given domain as domain part.
	 * 
	 * @param DomainInterface $domain
	 * @return MailboxGroupInterface
	 */
	public function filterByDomain(DomainInterface $domain) : MailboxGroupInterface;
	
	/**
	 * Gets the display name of the mailbox.
	 * 
	 * @return string
	 */
	public function getDisplayName() : string;
	
	/**
	 * Sets the display name of the mailbox group.
	 * 
	 * @param string $display
	 * @return MailboxGroupInterface
	 */
	public function withDisplayName(string $display) : MailboxGroupInterface;
	
	/**
	 * Adds an email address to this list.
	 *
	 * @param EmailAddressInterface $email
	 * @param ?string $display
	 * @return MailboxGroupInterface
	 */
	public function withEmailAddress(EmailAddressInterface $email, ?string $display = null) : MailboxGroupInterface;
	
	/**
	 * Adds all the email addresses to this list.
	 *
	 * @param EmailAddressListInterface $addressList
	 * @return MailboxGroupInterface
	 */
	public function withEmailAddressList(EmailAddressListInterface $addressList) : MailboxGroupInterface;
	
	/**
	 * Adds a mailbox to this list.
	 *
	 * @param MailboxInterface $mailbox
	 * @return MailboxGroupInterface
	 */
	public function withMailbox(MailboxInterface $mailbox) : MailboxGroupInterface;
	
	/**
	 * Adds all the mailboxes to this list.
	 *
	 * @param MailboxListInterface $mailboxList
	 * @return MailboxGroupInterface
	 */
	public function withMailboxList(MailboxListInterface $mailboxList) : MailboxGroupInterface;
	
	/**
	 * Gets the inner mailbox list that lies under this mailbox group.
	 * 
	 * @return MailboxListInterface
	 */
	public function getMailboxList() : MailboxListInterface;
	
	/**
	 * Gets a perfectly well quoted string that correspond to a canonical
	 * representation of this mailbox list.
	 *
	 * @return string
	 */
	public function getCanonicalRepresentation() : string;
	
	/**
	 * Gets whether this address list is empty.
	 *
	 * @return boolean
	 */
	public function isEmpty() : bool;
	
	/**
	 * Collects all the display names used by the mailboxes.
	 *
	 * @return Iterator<string>
	 */
	public function collectDisplayNames() : Iterator;
	
	/**
	 * Collects all the domains that are used by the email addresses.
	 *
	 * @return Iterator<integer, DomainInterface>
	 */
	public function collectDomains() : Iterator;
	
	/**
	 * Collects all the email addresses without the display names.
	 *
	 * @return EmailAddressListInterface
	 */
	public function collectEmailAddresses() : EmailAddressListInterface;
	
	/**
	 * Gets whether this mailbox group equals another mailbox group,
	 * meaning all email addresses of this list are in the other list, and
	 * vice-versa, regardless of the grouping and the labelling.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $other
	 * @return boolean
	 */
	public function equals($other) : bool;
	
	/**
	 * Gets whether this mailbox group contains the given email address.
	 *
	 * @param EmailAddressInterface $address
	 * @return boolean
	 */
	public function containsEmailAddress(EmailAddressInterface $address) : bool;
	
	/**
	 * Gets whether this mailbox group contains all the email addresses
	 * of the given email address list.
	 *
	 * @param EmailAddressListInterface $addressList
	 * @return boolean
	 */
	public function containsEmailAddressList(EmailAddressListInterface $addressList) : bool;
	
	/**
	 * Gets whether this mailbox group contains the given mailbox.
	 * 
	 * @param MailboxInterface $mailbox
	 * @return boolean
	 */
	public function containsMailbox(MailboxInterface $mailbox) : bool;
	
	/**
	 * Gets whether this mailbox group contains the email address of the
	 * given mailbox.
	 * 
	 * @param MailboxInterface $mailbox
	 * @return boolean
	 */
	public function containsMailboxIgnoreLabels(MailboxInterface $mailbox) : bool;
	
	/**
	 * Gets whether this mailbox group contains the mailboxes of the given
	 * mailbox list.
	 * 
	 * @param MailboxListInterface $mailboxList
	 * @return boolean
	 */
	public function containsMailboxList(MailboxListInterface $mailboxList) : bool;
	
	/**
	 * Gets whether this mailbox group contains the email addresses of the
	 * given mailbox list.
	 * 
	 * @param MailboxListInterface $mailboxList
	 * @return boolean
	 */
	public function containsMailboxListIgnoreLabels(MailboxListInterface $mailboxList) : bool;
	
	/**
	 * Gets whether this mailbox group contains the mailboxes of the
	 * given mailbox group.
	 * 
	 * @param MailboxGroupInterface $mailboxGroup
	 * @return boolean
	 */
	public function containsMailboxGroup(MailboxGroupInterface $mailboxGroup) : bool;
	
	/**
	 * Gets whether this mailbox group contains the email addresses of the
	 * given mailbox group.
	 * 
	 * @param MailboxGroupInterface $mailboxGroup
	 * @return boolean
	 */
	public function containsMailboxGroupIgnoreLabels(MailboxGroupInterface $mailboxGroup) : bool;
	
}

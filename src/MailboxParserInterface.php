<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use PhpExtended\Parser\ParserInterface;

/**
 * MailboxParserInterface interface file.
 * 
 * This interface represents a parser for a single mailbox data.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\ParserInterface<MailboxInterface>
 */
interface MailboxParserInterface extends ParserInterface
{
	
	// nothing to add
	// php does not accepts covariant return types between interfaces
	
}

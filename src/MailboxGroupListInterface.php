<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use Countable;
use Iterator;
use PhpExtended\Domain\DomainInterface;
use Stringable;

/**
 * MailboxGroupListInterface interface file.
 * 
 * This interface specifies how a list of mailbox groups should be handled.
 * 
 * Mailbox lists are considered immutable; all methods that might change state
 * MUST be implemented such that they retain the internal state of the current
 * list and return an instance that contains the changed state.
 * 
 * @author Anastaszor
 * @extends \Iterator<int, MailboxGroupInterface>
 */
interface MailboxGroupListInterface extends Countable, Iterator, Stringable
{
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 */
	public function current() : MailboxGroupInterface;
	
	/**
	 * Creates a new MailboxGroupInterface with all the email address domains
	 * converted to the given domain.
	 * 
	 * @param DomainInterface $domain
	 * @return MailboxGroupListInterface
	 */
	public function withDomain(DomainInterface $domain) : MailboxGroupListInterface;
	
	/**
	 * Creates a new MailboxGroupListInterface where all the email address are
	 * the email addresses which have the given domain as domain part.
	 * 
	 * @param DomainInterface $domain
	 * @return MailboxGroupListInterface
	 */
	public function filterByDomain(DomainInterface $domain) : MailboxGroupListInterface;
	
	/**
	 * Adds an email address to this list.
	 * 
	 * @param EmailAddressInterface $email
	 * @param ?string $display
	 * @param ?string $group
	 * @return MailboxGroupListInterface
	 */
	public function withEmailAddress(EmailAddressInterface $email, ?string $display = null, ?string $group = null) : MailboxGroupListInterface;
	
	/**
	 * Adds all the email addresses to this list.
	 * 
	 * @param EmailAddressListInterface $addressList
	 * @param ?string $group
	 * @return MailboxGroupListInterface
	 */
	public function withEmailAddressList(EmailAddressListInterface $addressList, ?string $group = null) : MailboxGroupListInterface;
	
	/**
	 * Adds a mailbox to this list.
	 * 
	 * @param MailboxInterface $mailbox
	 * @param ?string $group
	 * @return MailboxGroupListInterface
	 */
	public function withMailbox(MailboxInterface $mailbox, ?string $group = null) : MailboxGroupListInterface;
	
	/**
	 * Adds all the mailboxes to this list.
	 * 
	 * @param MailboxListInterface $mailboxList
	 * @param ?string $group
	 * @return MailboxGroupListInterface
	 */
	public function withMailboxList(MailboxListInterface $mailboxList, ?string $group = null) : MailboxGroupListInterface;
	
	/**
	 * Adds a mailbox group to this list.
	 * 
	 * @param MailboxGroupInterface $group
	 * @return MailboxGroupListInterface
	 */
	public function withMailboxGroup(MailboxGroupInterface $group) : MailboxGroupListInterface;
	
	/**
	 * Adds all the mailbox groups to this list.
	 * 
	 * @param MailboxGroupListInterface $mailboxGroupList
	 * @return MailboxGroupListInterface
	 */
	public function withMailboxGroupList(MailboxGroupListInterface $mailboxGroupList) : MailboxGroupListInterface;
	
	/**
	 * Gets a perfectly well quoted string that correspond to a canonical
	 * representation of this mailbox group list.
	 * 
	 * @return string
	 */
	public function getCanonicalRepresentation() : string;
	
	/**
	 * Gets whether this address list is empty.
	 * 
	 * @return boolean
	 */
	public function isEmpty() : bool;
	
	/**
	 * Collects all the display names used  by the mailboxes and groups.
	 *
	 * @return Iterator<string>
	 */
	public function collectDisplayNames() : Iterator;
	
	/**
	 * Collects all the domains that are used by the email addresses.
	 *
	 * @return Iterator<integer, DomainInterface>
	 */
	public function collectDomains() : Iterator;
	
	/**
	 * Collects all the email addresses without the display names.
	 * 
	 * @return EmailAddressListInterface
	 */
	public function collectEmailAddresses() : EmailAddressListInterface;
	
	/**
	 * Collects all the mailboxes.
	 * 
	 * @return MailboxListInterface
	 */
	public function collectMailboxes() : MailboxListInterface;
	
	/**
	 * Gets whether this mailbox group list equals another mailbox group list,
	 * meaning all email addresses of this list are in the other list, and
	 * vice-versa, regardless of the grouping and the labelling.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $other
	 * @return boolean
	 */
	public function equals($other) : bool;
	
	/**
	 * Gets whether this mailbox group list contains the given email address.
	 *
	 * @param EmailAddressInterface $address
	 * @return boolean
	 */
	public function containsEmailAddress(EmailAddressInterface $address) : bool;
	
	/**
	 * Gets whether this mailbox group list contains all the email addresses
	 * of the given email address list.
	 *
	 * @param EmailAddressListInterface $addressList
	 * @return boolean
	 */
	public function containsEmailAddressList(EmailAddressListInterface $addressList) : bool;
	
	/**
	 * Gets whether this mailbox group list contains the given mailbox.
	 * 
	 * @param MailboxInterface $mailbox
	 * @return boolean
	 */
	public function containsMailbox(MailboxInterface $mailbox) : bool;
	
	/**
	 * Gets whether this mailbox group list contains the email address of the
	 * given mailbox.
	 * 
	 * @param MailboxInterface $mailbox
	 * @return boolean
	 */
	public function containsMailboxIgnoreLabels(MailboxInterface $mailbox) : bool;
	
	/**
	 * Gets whether this mailbox group list contains the mailboxes of the given
	 * mailbox list.
	 * 
	 * @param MailboxListInterface $mailboxList
	 * @return boolean
	 */
	public function containsMailboxList(MailboxListInterface $mailboxList) : bool;
	
	/**
	 * Gets whether this mailbox group list contains the email addresses of the
	 * given mailbox list.
	 * 
	 * @param MailboxListInterface $mailboxList
	 * @return boolean
	 */
	public function containsMailboxListIgnoreLabels(MailboxListInterface $mailboxList) : bool;
	
	/**
	 * Gets whether this mailbox group list contains the mailboxes of the
	 * given mailbox group.
	 * 
	 * @param MailboxGroupInterface $mailboxGroup
	 * @return boolean
	 */
	public function containsMailboxGroup(MailboxGroupInterface $mailboxGroup) : bool;
	
	/**
	 * Gets whether this mailbox group list contains the email addresses of the
	 * given mailbox group.
	 * 
	 * @param MailboxGroupInterface $mailboxGroup
	 * @return boolean
	 */
	public function containsMailboxGroupIgnoreLabels(MailboxGroupInterface $mailboxGroup) : bool;
	
	/**
	 * Gets whether this mailbox group list contains the mailbox groups of the
	 * given mailbox group list.
	 * 
	 * @param MailboxGroupListInterface $mailboxGroupList
	 * @return boolean
	 */
	public function containsMailboxGroupList(MailboxGroupListInterface $mailboxGroupList) : bool;
	
	/**
	 * Gets whether this mailbox group list contains the addresses email of the
	 * given mailbox group list.
	 * 
	 * @param MailboxGroupListInterface $mailboxGroupList
	 * @return boolean
	 */
	public function containsMailboxGroupListIgnoreLabels(MailboxGroupListInterface $mailboxGroupList) : bool;
	
}

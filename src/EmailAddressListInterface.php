<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use Countable;
use Iterator;
use PhpExtended\Domain\DomainInterface;
use Stringable;

/**
 * EmailAddressListInterface interface file.
 * 
 * This interface specifies how a list of email addresses should be handled.
 * 
 * Email address lists are considered immutable; all methods that might change
 * state MUST be implemented such that they retain the internal state of the
 * current list and return an instance that contains the changed state.
 * 
 * @author Anastaszor
 * @extends \Iterator<int, EmailAddressInterface>
 */
interface EmailAddressListInterface extends Countable, Iterator, Stringable
{
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 */
	public function current() : EmailAddressInterface;
	
	/**
	 * Creates a new list where all the email address are converted to the
	 * given domain.
	 *
	 * @param DomainInterface $domain
	 * @return EmailAddressListInterface
	 */
	public function withDomain(DomainInterface $domain) : EmailAddressListInterface;
	
	/**
	 * Creates a new list where all the email address are the email address
	 * which have the given domain as domain part.
	 * 
	 * @param DomainInterface $domain
	 * @return EmailAddressListInterface
	 */
	public function filterByDomain(DomainInterface $domain) : EmailAddressListInterface;
	
	/**
	 * Adds an email address to this list.
	 * 
	 * @param EmailAddressInterface $address
	 * @return EmailAddressListInterface
	 */
	public function withEmailAddress(EmailAddressInterface $address) : EmailAddressListInterface;
	
	/**
	 * Adds all the email addresses of the given list to this list.
	 * 
	 * @param EmailAddressListInterface $addressList
	 * @return EmailAddressListInterface
	 */
	public function withEmailAddressList(EmailAddressListInterface $addressList) : EmailAddressListInterface;
	
	/**
	 * Gets whether this address list is empty.
	 * 
	 * @return boolean
	 */
	public function isEmpty() : bool;
	
	/**
	 * Gets a perfectly well quoted string that correspond to a canonical
	 * representation of this email address list.
	 * 
	 * @return string
	 */
	public function getCanonicalRepresentation() : string;
	
	/**
	 * Collects all the domains that are used by the email addresses.
	 * 
	 * @return Iterator<integer, DomainInterface>
	 */
	public function collectDomains() : Iterator;
	
	/**
	 * Gets whether this address list equals another address list, meaning all
	 * the addresses of this list are in the other list, and vice-versa.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $other
	 * @return boolean
	 */
	public function equals($other) : bool;
	
	/**
	 * Gets whether this email address list contains the given email address.
	 * 
	 * @param EmailAddressInterface $address
	 * @return boolean
	 */
	public function containsEmailAddress(EmailAddressInterface $address) : bool;
	
	/**
	 * Gets whether this email address list contains all the email addresses
	 * of the given email address list.
	 * 
	 * @param EmailAddressListInterface $addressList
	 * @return boolean
	 */
	public function containsEmailAddressList(EmailAddressListInterface $addressList) : bool;
	
}

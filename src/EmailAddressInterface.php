<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use InvalidArgumentException;
use PhpExtended\Domain\DomainInterface;
use PhpExtended\Ip\Ipv4AddressInterface;
use PhpExtended\Ip\Ipv6AddressInterface;
use Stringable;

/**
 * EmailAddressInterface interface file.
 * 
 * This interface specifies how an email address should be handled.
 * 
 * Email Addresses are considered immutable; all methods that might change state
 * MUST be implemented such that they retain the internal state of the current
 * address and return an instance that contains the changed state.
 * 
 * @author Anastaszor
 */
interface EmailAddressInterface extends Stringable
{
	
	/**
	 * Gets the local part of the email address.
	 * 
	 * @return string
	 */
	public function getLocalPart() : string;
	
	/**
	 * Sets the local part of the email address. If the local part is not
	 * conform to rfc5322 (3.4.1 Addr-Spec), then an InvalidArgumentException
	 * is thrown.
	 * 
	 * @param string $local
	 * @return EmailAddressInterface
	 * @throws InvalidArgumentException if the local part is not acceptable
	 */
	public function withLocalPart(string $local) : EmailAddressInterface;
	
	/**
	 * Gets whether this email address uses an ip address (ipv4 or ipv6) as a
	 * domain to route to its mailbox.
	 * 
	 * @return boolean
	 */
	public function hasIpAsDomain() : bool;
	
	/**
	 * Gets the domain of the email address.
	 * 
	 * If a named domain does not exists, this method will return an arpa
	 * domain based on the ip address of given to this email address.
	 * 
	 *  To know if the domain is a real domain, use the hasIpAsDomain() method.
	 * 
	 * @return DomainInterface
	 */
	public function getDomain() : DomainInterface;
	
	/**
	 * Sets the domain part of the email address. If the domain part is not
	 * conform to rfc5322 (3.4.1 Addr-Spec), then an InvalidArgumentException
	 * is thrown.
	 * 
	 * Setting the domain for this email address removes any previous domain,
	 * ipv4 or ipv6 it uses has route.
	 * 
	 * @param DomainInterface $domain
	 * @return EmailAddressInterface
	 */
	public function withDomain(DomainInterface $domain) : EmailAddressInterface;
	
	/**
	 * Gets the ipv4 of this domain, null if no ipv4 are used as domain.
	 * 
	 * @return ?Ipv4AddressInterface
	 */
	public function getIpv4Address() : ?Ipv4AddressInterface;
	
	/**
	 * Sets the domain as ipv4 for this email.
	 * 
	 * Setting the domain for this email address removes any previous domain,
	 * ipv4 or ipv6 it uses has route.
	 * 
	 * @param Ipv4AddressInterface $address
	 * @return EmailAddressInterface
	 */
	public function withIpv4Address(Ipv4AddressInterface $address) : EmailAddressInterface;
	
	/**
	 * Gets the ipv6 of this domain, null if no ipv6 are used as domain.
	 * 
	 * @return ?Ipv6AddressInterface
	 */
	public function getIpv6Address() : ?Ipv6AddressInterface;
	
	/**
	 * Sets the domain as ipv6 for this email.
	 * 
	 * Setting the domain for this email address removes any previous domain,
	 * ipv4 or ipv6 it uses has route.
	 * 
	 * @param Ipv6AddressInterface $address
	 * @return EmailAddressInterface
	 */
	public function withIpv6Address(Ipv6AddressInterface $address) : EmailAddressInterface;
	
	/**
	 * Gets a perfectly well quoted string that correspond to a canonical 
	 * representation of this email address. As email addresses are case 
	 * insensitive, this email address is returned in full lowercase.
	 * 
	 * @return string
	 */
	public function getCanonicalRepresentation() : string;
	
	/**
	 * Gets whether this email address equals another email address, meaning
	 * they have the same local part and the same domain.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $other
	 * @return boolean true if both objects are equals
	 */
	public function equals($other) : bool;
	
}
